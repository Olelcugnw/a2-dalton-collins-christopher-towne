package box;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
	//reads the csv data file and performs the algorithm and visualization
	public static void main(String[] args) throws IOException{
		String csvf = "C:\\Users\\49904\\workspace2\\test\\src\\box\\aNum.csv";
		BufferedReader br = null;
		String line = "";
		FileReader fr = new FileReader(csvf);
		br = new BufferedReader(fr);
		HashSet<Point> data = new HashSet<Point>();
		br.readLine();
		
		while((line = br.readLine()) != null){
			
			String[] pt = line.split(",");
			Float px = (float)Integer.parseInt(pt[0]) / 150;
			Float py = (float)Integer.parseInt(pt[1]) / 84000;
			
			Point p = new Point(px, py);
			data.add(p);
		}
		fr = new FileReader("C:\\Users\\49904\\workspace2\\test\\src\\box\\theNum.csv");
		br = new BufferedReader(fr);
		br.readLine();
		
		while((line = br.readLine()) != null){
			
			String[] pt = line.split(",");
			Float px = ((float)Integer.parseInt(pt[0])) / 150;
			Float py = ((float)Integer.parseInt(pt[1])) / 84000;
			//System.out.println("datap x " + px + " y " + py);
			Point p = new Point(px, py);
			data.add(p);
		}
		fr.close();
		
		
		//K_Means_Doer kmd = new K_Means_Doer(data, 5);
		//HashMap<Point, ArrayList<Point>> means_Map = kmd.do_K_Means();
		//Visualizer V = new Visualizer(means_Map);
		
		K_Medoids_Doer kmd2 = new K_Medoids_Doer(data, 5);
		HashMap<Point, ArrayList<Point>> medoids_Map = kmd2.do_K_Medoids();
		Visualizer V = new Visualizer(medoids_Map);
		V.visualize();
	}
}
