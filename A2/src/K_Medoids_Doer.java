package box;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class K_Medoids_Doer {
   
    HashSet<Point> points;
    int n_Medoids;
    public K_Medoids_Doer(HashSet<Point> pointsss, int k_Medoids){
        points = pointsss;
        n_Medoids = k_Medoids;
    }
    //take in a set of points
    // assign k Medoids to random locations(or the first k points in your set)
    // loop
    // put each point in the medoid-group its closest to
    // for each medoid for each point switch them and recompute total distance
    //stop after 30 tries(bug catcher and time saver)
   
    ArrayList<Point> medoids_List = new ArrayList<Point>();
   
    public HashMap<Point, ArrayList<Point>> do_K_Medoids(){
       
        //set the starting Medoids
        int i = 0;
        for(Point p : points){
            if(i >= n_Medoids){
                break;
            }
            Point pn = new Point(p.getX(), p.getY());
            medoids_List.add(pn);
            i++;
        }
        //generate k groups
        HashMap<Point, ArrayList<Point>> medoids_Group_Map = new HashMap<Point, ArrayList<Point>>();
        for(Point p : medoids_List){
            ArrayList<Point> group = new ArrayList<Point>();
            medoids_Group_Map.put(p, group);
        }
        ArrayList<Point> old_Medoids = new ArrayList<Point>();
        int loop_count =0;
        while(medoids_List != old_Medoids){
        	//assign the points to a medoids group
        	for(Point p : points){
        		Float min_dist = (float)1000000;
        		ArrayList<Float> distances = new ArrayList<Float>();
        		for(Point m : medoids_List){
        			Float distance = p.distance_To(m);
        			distances.add(distance);
        			if(distance < min_dist){
        				min_dist = distance;
        			}
        		}
        		Point closest_Medoid = medoids_List.get(distances.indexOf(min_dist));
        		medoids_Group_Map.get(closest_Medoid).add(p);
        	}
        	//swap each medoid with each point in its group and recompute total distance
        	ArrayList<Point> temp_Medoids_List = new ArrayList<Point>();
        	HashMap<Point, ArrayList<Point>> temp_Group_Map = new HashMap<Point, ArrayList<Point>>();
        	
        	for(Point m : medoids_Group_Map.keySet()){
        		Point cheapest_Point = m;
            	Float min_Distance = compute_Total_Distance(m, medoids_Group_Map.get(m));
            	for(Point p : medoids_Group_Map.get(m)){
            		ArrayList<Point> tempGroup = new ArrayList<Point>(medoids_Group_Map.get(m));
                	tempGroup.add(m);
                	tempGroup.remove(p);
                	Float dist = compute_Total_Distance(p, tempGroup);
            		if(dist < min_Distance){
            			cheapest_Point = p;
            			min_Distance = dist;
            		}
            	}
            	//update the temp medoids list and group map
            	
            	temp_Medoids_List.add(cheapest_Point);
            	temp_Group_Map.put(cheapest_Point, medoids_Group_Map.get(m));
        	}
        	medoids_List = temp_Medoids_List;
        	medoids_Group_Map = temp_Group_Map;
        	loop_count++;
        	if(loop_count > 20){
        		break;
        	}
        	//clear the groups
        	for(ArrayList<Point> g : medoids_Group_Map.values()){
        		g.clear();
        	}
        	int id = 1;
    		for(Point p : medoids_List){
    			System.out.println("Mean " + id + " " +  p.getX() + " " + p.getY());
    			id++;
    		}
        }
        return medoids_Group_Map;
    }
    
    Float compute_Total_Distance(Point m, ArrayList<Point> g){
    	Float total_Distance = (float) 0;
    	for(Point p : g){
    		total_Distance += p.distance_To(m);
    	}
    	return total_Distance;
    }
}
