package box;

public class Point {
    Float x;
    Float y;
   
    public Point(Float xs, Float ys){
        x = xs;
        y = ys;
    }
   
    public Float getX(){
        return x;
    }
    public Float getY(){
        return y;
    }
   
    public Float distance_To(Point p ){
        Float xd = (float) Math.pow((p.getX() - x) / 2 , 2);
        Float yd = (float) Math.pow((p.getY() - y) / 2 , 2);
        return (float) Math.pow(xd + yd, 0.5);
    }
}