package box;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class K_Means_Doer {
   
    HashSet<Point> points;
    int n_means;
    public K_Means_Doer(HashSet<Point> pointsss, int n_meansss){
        points = pointsss;
        n_means = n_meansss;
    }
    //take in a set of points
    // assign k means to random locations(or the first k points in your set)
    // loop
    // set each point to the mean its closest to
    // move the means to the new mean of its group
    //stop after 30 tries(bug catcher and time saver)
   
    ArrayList<Point> means_List = new ArrayList<Point>();
   
    public HashMap<Point, ArrayList<Point>> do_K_Means(){
       
        //set the starting means
        int i = 0;
        for(Point p : points){
            if(i >= n_means){
                break;
            }
            Point pn = new Point(p.getX(), p.getY());
            means_List.add(pn);
            i++;
        }
        //generate k groups
        HashMap<Point, ArrayList<Point>> mean_Group_Map = new HashMap<Point, ArrayList<Point>>();
        for(Point p : means_List){
            ArrayList<Point> group = new ArrayList<Point>();
            mean_Group_Map.put(p, group);
        }
        ArrayList<Point> old_means = new ArrayList<Point>();
        int loop_count =0;
        
        while(means_List != old_means){
        	
            for(Point p : points){
            	//System.out.println("point x" + p.getX() + " y " + p.getY());
            	//get the distances to all means from p
                ArrayList<Float> distances = new ArrayList<Float>();
                for(Point m : means_List){
                   distances.add(p.distance_To(m));
                   
                }
                //find the closest mean to point p
                Float min_dist = (float) 10000000;
                for(Float f : distances){
                	if(f < min_dist){
                		min_dist = f;
                	}
                }
                //add point p to the closest mean's group
                int ind = distances.indexOf(min_dist);
                Point closest_Mean = means_List.get(ind);
                mean_Group_Map.get(closest_Mean).add(p);
            }
            //remember the old means before assigning new ones
            if(old_means.isEmpty()){
            	for(Point P : means_List){
            		old_means.add(P);
            	}
            }
            for(Point P : means_List){
            	old_means.set(means_List.indexOf(P), P);
            }
            //move the means to the center of their group
            for(Point m : means_List){
            	//find new mean of the group
            	Float totalX = (float) 0;
            	Float totalY = (float) 0;
            	for(Point pm : mean_Group_Map.get(m)){
            		totalX+= pm.getX();
            		totalY+= pm.getY();
            	}
            	Float meanX = totalX / (float)mean_Group_Map.get(m).size();
            	Float meanY = totalY / (float)mean_Group_Map.get(m).size();
            	Point new_Mean = new Point(meanX, meanY);
            	//change the list and map means to the new ones
            	means_List.set(means_List.indexOf(m), new_Mean);
            	mean_Group_Map.put(new_Mean, mean_Group_Map.get(m));
            	mean_Group_Map.remove(m);
            }
            loop_count+=1;
        	if(loop_count >20){
        		break;
        	}
            //clear mean groups
            for(ArrayList<Point> g : mean_Group_Map.values()){
            	g.clear();
            }
            
        }
        return mean_Group_Map;
    }
}
