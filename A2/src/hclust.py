#A template for the implementation of hclust
import math #sqrt
import scipy.cluster.hierarchy as hy
import numpy
import matplotlib.pyplot as plt
import random
# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    sumThing = 0
    for i in range(2):
        sumThing = sumThing + math.pow(a[i]-b[i],2)  
    return math.sqrt(sumThing)

# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b, indexI):
    newPoint =  [None] * 5
    for i in range(2):
        newPoint[i] = (a[i] + b[i]) /2
    newPoint[2] = Distance(a,b)
    #number of sub points
    newPoint[3] = a[3]+b[3]
    newPoint[4] = indexI
    return newPoint

# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(centers):
    D=iter(centers)
    E=iter(centers)
    first = next(D)
    second = next(D)
    #print(first, second)
    minDist = Distance(centers[first], centers[second])#Distance(D[0], D[1])
    location = [centers[first], centers[second]]
    location[1]
    #print("seperater")
    for c in D:
        for d in E:
            if d != c:
                #print(c,d, type(c), type(d))
                d1 = centers[d]
                c1 = centers[c]
                #c, d = centers[c], centers[d]
                #print(c,d)
                if Distance(c1,d1) < minDist:
                    minDist = Distance(c1,d1)
                    location = [c1,d1]
    return location

# Accepts a list of data points.
# Produces a tree structure corresponding to a 
# Agglomerative Hierarchal clustering of D.

#Data is in format [length of tweet, time of tweet, distance if is merged, number of points within the point, and id number]
def HClust(D):
    Z = numpy.empty(shape=(len(D)-1,4))
    indexI = 0
    centers = dict()#D[0:len(D)]
    for item in D:
        centers[item[4]] = item
    #splits = []
    location = findClosestPair(centers)
    del centers[location[0][4]]#centers = [item for item in centers if item not in location[1] and not item is location[1]]
    del centers[location[1][4]]#centers = [item for item in centers if item not in location[1] and not item is location[0]]
    #centers[location[0]] = merge(location[0], location[1])
    newPoint =  merge(location[0], location[1], len(D)+indexI)
    Z[indexI, 0] = location[0][4]
    Z[indexI, 1] = location[1][4]
    Z[indexI, 2] = newPoint[2]
    Z[indexI, 3] = newPoint[3] 
    centers[newPoint[4]] = newPoint#centers.append(newPoint)
    #splits.append([newPoint, location])
    indexI = indexI +1
    
    while len(centers) > 1:
        location = findClosestPair(centers)
        del centers[location[0][4]]#centers = [item for item in centers if item not in location[1] and not item is location[1]]
        del centers[location[1][4]]#centers = [item for item in centers if item not in location[1] and not item is location[0]]
        #centers[location[0]] = merge(location[0], location[1])
        newPoint =  merge(location[0], location[1], len(D)+indexI)
        Z[indexI, 0], Z[indexI, 1], Z[indexI, 2], Z[indexI, 3] = location[0][4], location[1][4], newPoint[2], newPoint[3]
        centers[newPoint[4]] = newPoint#centers.append(newPoint)
        #splits.append([newPoint, location])
        indexI = indexI +1
    print(indexI)
    #print(splits, centers)
    #print_tree(centers[0])
    return Z, D
    
def proccess(filename, filename1):
    f = open(filename, 'r')
    f1 = open(filename1, 'r')
    D = []
    f.readline()
    f1.readline()
    for line in f:
        D.append(line[0:len(line)-1].split(","))
    for line in f1:
        D.append(line[0:len(line)-1].split(","))
    print(len(D),D)
    
    #Activates subsampling
    D = random.sample(D, 400)
    
    for i in range(len(D)):
        for j in range(2):
            D[i][j] = float(D[i][j]) 
        D[i].append(0)
        D[i].append(1)
        D[i].append(i)
    
    columnMean = [0] * 2
    columnMax = [0] * 2
    columnMin = [float("inf")] * 2
    for i in range(len(D)):
        for j in range(2):
            columnMean[j] = columnMean[j] + D[i][j]
            if columnMax[j] < D[i][j]: columnMax[j] = D[i][j]
            if columnMin[j] > D[i][j]: columnMin[j] = D[i][j]
    for i in range(len(columnMean)): columnMean[i] = columnMean[i] / len(D)
    for i in range(len(D)):
        for j in range(2):
            D[i][j] = (D[i][j] - columnMean[j]) / (columnMax[j] - columnMin[j])
    
    return D
    
#print("a", sep=' ', end='\n')
#print(proccess('wine.data'))
Z, D = HClust(proccess('aNum.csv', "theNum.csv"))
print(Z)

R = hy.dendrogram(Z, leaf_font_size =10)
plt.title("dendrogram")
plt.show()
leaf = R["leaves"]
length = []
sec = []
for i in range(len(leaf)):
    print(D[leaf[i]],end='')
    length.append(D[leaf[i]][0])
    sec.append(D[leaf[i]][1])
plt.bar(range(len(length)),length)
plt.title("Spread: Length of tweet")
plt.show()
plt.bar(range(len(sec)),sec)
plt.title("Spread: Time of tweet in seconds")
plt.show()
'''
Z = numpy.matrix([[0,1,3,2],[2,3,2,2],[4,5,7,4]])
print(hy.is_valid_linkage(Z.astype(float)))
hy.dendrogram(Z.astype(float))
'''