package box;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;

public class Visualizer extends Applet{
	
	HashMap<Point, ArrayList<Point>> graph;
	public Visualizer(HashMap<Point, ArrayList<Point>> hm){
		graph = hm;
		for(Point m : graph.keySet()){
			//System.out.println("means " + m.getX() + " " + m.getY());
			for(Point p : graph.get(m)){
				//System.out.println("point " + p.getX() + " " + p.getY());
			}
		}
	}
	public void paint(Graphics g){
		for(Point m : graph.keySet()){
			int mx = Math.round(m.getX()*500);
			int my = Math.round(m.getY()*500);
			//System.out.println("mx " + mx + " my " + my);
			for(Point p : graph.get(m)){
				int px = Math.round(p.getX()*500);
				int py = Math.round(p.getY()*500);
				//System.out.println("x " + px + " y " + py);
				g.drawLine(mx, my, px, py);
				g.drawRoundRect(px-5, py-5, 9, 9, 6, 6);
			}
		}
	}
	
	public void visualize(){

        JFrame jf = new JFrame();
        jf.getContentPane().add(this, BorderLayout.CENTER);
        jf.setSize(new Dimension(5000,5000));
        jf.setVisible(true);

    }
}
