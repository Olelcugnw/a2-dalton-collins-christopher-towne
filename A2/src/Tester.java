package box;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class Tester {

	public static void main(String[] args) {
		
		HashSet<Point> data = new HashSet<Point>();
		//these add points to the data, the last section is random
		for(Float i = (float) 1; i < 10; i++){
			Point p = new Point(i, (float) 0);
			data.add(p);
		}
		
		for(Float j = (float) 1; j < 10; j++){
			Point p = new Point((float) 0, j);
			data.add(p);
		}
		Random rand = new Random();
		for(int i = 0; i < 20; i++){
			int x = rand.nextInt(15);
			int y = rand.nextInt(15);
			Point randm = new Point((float)x, (float)y);
			data.add(randm);
		}
		System.out.println("made the data");
		K_Means_Doer kmd = new K_Means_Doer(data, 4);
		//K_Medoids_Doer kmd = new K_Medoids_Doer(data, 4);
		HashMap<Point, ArrayList<Point>> means_Map = kmd.do_K_Means();
		Visualizer V = new Visualizer(means_Map);
		V.visualize();
		
		
	}

}
