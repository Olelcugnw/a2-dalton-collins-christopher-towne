#A template for the implementation of hclust
import math #sqrt


class node:
    value = 0
    left  = None
    right = None
    def __init__(self, value = None, left = None, right = None):
        self.value = value
        self.left = left
        self.right = right
        
#modified professor's kalmonovich's code
def print_tree(p,depth=0):
    # prints tree sideways
    if (p[14].right!=None):
        print_tree(p[14].right, depth+1)
    print("    "*depth+str(p[0]), p[15])
    print("\n"*int(math.floor(p[15])), end="")
    if (p[14].left !=None):
        print_tree(p[14].left , depth+1)
# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    sumThing = 0
    for i in range(1, 14):
        sumThing = sumThing + math.pow(a[i]-b[i],2)  
    return math.sqrt(sumThing)

# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b):
    newPoint =  [None] * 16
    for i in range(14):
        newPoint[i] = (a[i] + b[i]) /2
    newPoint[14] = node(newPoint, a, b)
    newPoint[15] = Distance(a,b)
    return newPoint

# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
    minDist = Distance(D[0], D[1])
    location = [D[0], D[1]]
    location[1]
    for c in D:
        for d in ([item for item in D if item not in c and not item is c]):
            if Distance(c,d) < minDist:
                minDist = Distance(c,d)
                location = [c,d]
    return location

# Accepts a list of data points.
# Produces a tree structure corresponding to a 
# Agglomerative Hierarchal clustering of D.
def HClust(D):
    centers = D[1:len(D)]
    splits = []
    location = findClosestPair(centers)
    centers = [item for item in centers if item not in location[1] and not item is location[1]]
    centers = [item for item in centers if item not in location[1] and not item is location[0]]
    #centers[location[0]] = merge(location[0], location[1])
    newPoint =  merge(location[0], location[1])
    centers.append(newPoint)
    #splits.append([newPoint, location])
    
    while len(centers) > 1:
        location = findClosestPair(centers)
        centers = [item for item in centers if item not in location[1] and not item is location[1]]
        centers = [item for item in centers if item not in location[1] and not item is location[0]]
        #centers[location[0]] = merge(location[0], location[1])
        newPoint =  merge(location[0], location[1])
        centers.append(newPoint)
        #splits.append([newPoint, location])
    print(splits, centers)
    print_tree(centers[0])
    
def proccess(filename):
    f = open(filename, 'r')
    D = []
    for line in f:
        D.append(line[0:len(line)-1].split(","))
    for i in range(len(D)):
        for j in range(14):
            D[i][j] = float(D[i][j]) 
            D[i].append(node(D[i][j]))
            D[i].append(0)
    columnMean = [0] * 14
    columnMax = [0] * 14
    columnMin = [float("inf")] * 14
    for i in range(len(D)):
        for j in range(1,14):
            columnMean[j] = columnMean[j] + D[i][j]
            if columnMax[j] < D[i][j]: columnMax[j] = D[i][j]
            if columnMin[j] > D[i][j]: columnMin[j] = D[i][j]
    for i in range(len(columnMean)): columnMean[i] = columnMean[i] / len(D)
    for i in range(len(D)):
        for j in range(1,14):
            D[i][j] = (D[i][j] - columnMean[j]) / (columnMax[j] - columnMin[j])
    return D
    
#import sys
#print(sys.version)
#print("a", sep=' ', end='\n')
#print(proccess('wine.data'))
HClust(proccess('wine.data'))