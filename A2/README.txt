KMeans, KMedians, and their visualization code are in Java. The only problem running them might be getting the project/package to not complain. Delete the line that calls the package box.
Hcluster is in python along with the visualization code. The only problem with that is that it requires scipy, but it is probably on the server.
The preproccessing script [getTwitterData2.R] is in R, the data scraping will not run unless you have the keys from your own twitter app (which is free). My keys are mine.
Pics are in Pics, and the report should be in the main folder.
The stuff in Other are just house keeping stuff. The Kmeans and Kmedians in there are just the templates.